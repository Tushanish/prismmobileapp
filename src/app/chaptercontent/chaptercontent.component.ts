import { Component, OnInit, ViewChild, ElementRef, Pipe, PipeTransform, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { ChaptercontentService } from '../services/chaptercontent/chaptercontent.service';
import { ChapterSection } from '../model/chaptersection.model';
import { Configuration } from '../configuration/config';
import { ChapterSectionContent } from '../model/chaptersectioncontent.model';
import { Observable, Subscription } from 'rxjs';
import { SwiperOptions } from 'swiper';
import { Sections } from '../model/section.model';
import { ChaptersectionService } from '../services/chaptersection/chaptersection.service';
import { stringify } from 'querystring';

import { DomSanitizer } from '@angular/platform-browser'
import { Location } from '@angular/common';
import { element } from 'protractor';

declare var jquery: any;
declare var $: any;

@Pipe({ name: 'safeHtml' })
export class SafeHtmlPipe implements PipeTransform {
  constructor(private sanitized: DomSanitizer) { }
  transform(value) {
    return this.sanitized.bypassSecurityTrustHtml(value);
  }
}

@Component({
  selector: 'app-chaptercontent',
  templateUrl: './chaptercontent.component.html',
  styleUrls: ['./chaptercontent.component.css']
})
export class ChaptercontentComponent implements OnInit, OnDestroy {
  @ViewChild("videoPlayer", { static: false }) videoplayer: ElementRef;
  isPlay: boolean = false;

  MediumNameFirstElement: string;
  SubjectNameFirstElement: string;
  ChapterNameFirstElement: string;
  SectionNameFirstElement: string;
  loading: boolean = true;
  ContentType: string;
  public fullScreenVideo: boolean;
  private chapterSectionParams: any;
  public baseUrl = Configuration.APIURL;

  chapterContentSection: ChapterSectionContent[];
  chapterSections: Sections[];

  private bgSectionColor = ['serviceBoxChapterSection', 'serviceBoxChapterSection purple',
    'serviceBoxChapterSection blue', 'serviceBoxChapterSection green']
  bgSectionColorIndex = 0;

  globalChapterSecgtion: Sections = null;
  sponsorerName: string = "";
  config: SwiperOptions = {
    pagination: { el: '.swiper-pagination', clickable: true },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    initialSlide: 0,
    spaceBetween: 10,
    slidesPerView: 1
  };


  constructor(private routes: Router,
    private routeParam: ActivatedRoute,
    private chapterContentService: ChaptercontentService,
    private location: Location,
    private chapterSectionService: ChaptersectionService) { }

  ngOnInit() {

    this.goEventHandler();

    let chapterId: string;
    let sectionId: string;

    if (this.getUserInfoDetails().SponsorerName != null) {
      this.sponsorerName = this.getUserInfoDetails().SponsorerName;
    }

    this.chapterSectionParams = this.routeParam.queryParams.subscribe(params => {
      chapterId = params["chapterId"];
      sectionId = params["sectionId"];
    });

    if (this.globalChapterSecgtion != null) {
      chapterId = this.globalChapterSecgtion.ChapterId;
      sectionId = this.globalChapterSecgtion.SectionId;
      this.globalChapterSecgtion = null;
    }

    this.chapterContentService.GetChapterSectionList(chapterId, sectionId)
      .subscribe(chapterContentSection => {
        this.chapterContentSection = chapterContentSection;
        this.loading = false;
        let mediumId = this.chapterContentSection[0].MediumId;
        let subjectId = this.chapterContentSection[0].SubjectId;
        this.chapterSectionService.GetChapterSectionList(true, mediumId, true, chapterId, subjectId)
          .subscribe(chapterSections => {
            this.AssignChapterIdToArray(chapterSections, chapterId);
            this.AssignChapterSectionBackGroundColor(chapterSections);
            this.chapterSections = chapterSections;
          });
      });


  }


  AssignChapterIdToArray(tempSection: Sections[], chapterId: string) {
    tempSection.map((obj) => {
      obj.ChapterId = chapterId;
      obj.FAIcons = this.getRandomFaIcons();
      return obj;
    })
  }

  getRandomFaIcons() {
    let fontIcons = ['fa fa-university', 'fa fa-bomb', 'fa fa-building', 'fa fa-child', 'fa fa-coffee', 'fa fa-cog', 'fa fa-cogs'];
    return fontIcons[Math.floor(Math.random() * fontIcons.length)];
  }

  getRandomBgChapterSectionColor() {
    if (this.bgSectionColorIndex == this.bgSectionColor.length) {
      this.bgSectionColorIndex = 0;
    }
    let bgTempColor = this.bgSectionColor[this.bgSectionColorIndex];
    this.bgSectionColorIndex = this.bgSectionColorIndex + 1;
    return bgTempColor;
  }

  AssignChapterSectionBackGroundColor(tempSection: Sections[]) {
    tempSection.map((obj) => {
      obj.ChapterSectionBgColor = this.getRandomBgChapterSectionColor();
      return obj;
    })
  }

  onSectionClick(chapterSection: Sections) {
    this.globalChapterSecgtion = chapterSection;
    this.ngOnInit();
  }

  goEventHandler() {
    document.addEventListener('backbutton', this.listener, false);

    // document.addEventListener("fullscreenchange", function (event) {
    //   if (document.fullscreenElement) {
    //     console.log("fullScreen Activated");

    //   } else {
    //     console.log("fullScreen deaactivated");
    //     // var elem = document.getElementById("my_video_1");
    //     $("#my_video_1")[0].pause();
    //   }
    // });
  }

  // toggleVideo() {
  //   alert("video toggle")
  //   //this.videoplayer.nativeElement.play();
  //   let elem = this.videoplayer.nativeElement as HTMLVideoElement;
  //   if (elem.requestFullscreen)
  //   {
  //     alert("FullScreen video")
  //     this.fullScreenVideo = true;
  //   }
  // }




  listener = () => {
    // alert("back buttonn  prss");
    if ($("#myModalContent").is(':visible')) {
      $('#myModalContent').modal('hide');
    }
    // else if (this.chapterContentSection[0].ContentType === "3" && this.fullScreenVideo) {
    //   //var elem = document.getElementById("my_video_1");
    //   console.log("Called cordova back button listner to exit full screen");
    //   document.exitFullscreen();
    //   //$("#my_video_1")[0].pause();
    //   alert("existing full screen mode");
    //   // this.location.back();
    // }
    else {
      // alert("going back");
      this.location.back();
    }
  }

  getUserInfoDetails() {
    var currentUserInfo = JSON.parse(localStorage.getItem('userinfo'));
    if (currentUserInfo == null) {
      this.routes.navigate(['']);
    }
    return currentUserInfo;
  }


  ngOnDestroy() {
    document.removeEventListener('backbutton', this.listener, false);
  }
}
