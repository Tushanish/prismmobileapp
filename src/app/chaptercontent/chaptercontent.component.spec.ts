import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChaptercontentComponent } from './chaptercontent.component';

describe('ChaptercontentComponent', () => {
  let component: ChaptercontentComponent;
  let fixture: ComponentFixture<ChaptercontentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChaptercontentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChaptercontentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
