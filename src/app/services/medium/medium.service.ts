import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import {Medium} from '../../model/medium.model';
import {Configuration} from '../../configuration/config';
@Injectable({
  providedIn: 'root'
})
export class MediumService {
  readonly url=Configuration.APIURL +'medium';
  listMedium: Medium[];  

  constructor(private http: HttpClient) { }

  GetMediumList() {  
    this.http.get(this.url + '/getmediumlist').toPromise().then(result=> this.listMedium= result as Medium[])  
  } 
}
