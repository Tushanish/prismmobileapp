import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Configuration } from 'src/app/configuration/config';
import {ChapterSectionContent} from '../../model/chaptersectioncontent.model'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChaptercontentService {
  readonly url=Configuration.APIURL +'chaptercontent';
  constructor(private http: HttpClient) { }

  GetChapterSectionList(chapterId:string,sectionId:string): Observable<ChapterSectionContent[]> {  
    let headers = new HttpHeaders();
    headers  = headers.append('responseType', 'json');
    let urlLocal:string;
    urlLocal=this.url + '/getchaptersectioncontent/'+chapterId+'/'+sectionId;
    return this.http.get<ChapterSectionContent[]>(urlLocal);
  } 
}
