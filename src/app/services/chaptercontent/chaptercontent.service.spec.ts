import { TestBed } from '@angular/core/testing';

import { ChaptercontentService } from './chaptercontent.service';

describe('ChaptercontentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChaptercontentService = TestBed.get(ChaptercontentService);
    expect(service).toBeTruthy();
  });
});
