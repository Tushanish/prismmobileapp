import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Configuration } from 'src/app/configuration/config';
import {Sections} from '../../model/section.model'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChaptersectionService {
  readonly url=Configuration.APIURL +'section';
  

  constructor(private http: HttpClient) { }

  GetChapterSectionList(isChapterSection:boolean,mediumId:string,isActive:boolean,chapterId:string,subjectId:string): Observable<Sections[]>  {  
    let headers = new HttpHeaders();
    headers  = headers.append('responseType', 'json');
    let urlLocal:string;
    urlLocal=this.url + '/getChapterSectionList/'+isChapterSection+'/'+mediumId+'/'+isActive+'/'+subjectId+'/'+chapterId;
    return this.http.get<Sections[]>(urlLocal);
  }
  
  
}
