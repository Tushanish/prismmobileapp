import { TestBed } from '@angular/core/testing';

import { ChaptersectionService } from './chaptersection.service';

describe('ChaptersectionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChaptersectionService = TestBed.get(ChaptersectionService);
    expect(service).toBeTruthy();
  });
});
