import { Injectable } from '@angular/core';
import {Subject } from '../../model/subject.model';
import { Configuration } from 'src/app/configuration/config';
import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {
  readonly url=Configuration.APIURL +'subject';

  constructor(private http: HttpClient) { 
    
  }

  GetSubjectList(loginId:string): Observable<Subject[]> {  
    let headers = new HttpHeaders();
    headers  = headers.append('responseType', 'json');
    let urlLocal:string;
    urlLocal=this.url + '/getsubjectlist/'+loginId;
    return this.http.get<Subject[]>(urlLocal);
  } 

 
}
