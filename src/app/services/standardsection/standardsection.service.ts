import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Configuration } from 'src/app/configuration/config';
import {Sections} from '../../model/section.model'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StandardsectionService {
  readonly url=Configuration.APIURL +'section';

  constructor(private http: HttpClient) { }

  GetStandardSectionList(isStandardSection:boolean,subjectId:string,mediumId:string,isActive:boolean)
  : Observable<Sections[]>  {  
    let headers = new HttpHeaders();
    headers  = headers.append('responseType', 'json');
    let urlLocal:string;
    urlLocal=this.url + '/getStandardSectionList/'+isStandardSection+'/'+subjectId+'/'+mediumId+'/'+isActive;
    return this.http.get<Sections[]>(urlLocal);
  } 

  
}
