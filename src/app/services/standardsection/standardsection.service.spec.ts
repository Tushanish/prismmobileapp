import { TestBed } from '@angular/core/testing';

import { StandardsectionService } from './standardsection.service';

describe('StandardsectionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StandardsectionService = TestBed.get(StandardsectionService);
    expect(service).toBeTruthy();
  });
});
