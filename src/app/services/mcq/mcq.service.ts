import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Configuration } from 'src/app/configuration/config';
import { ApiQuestion } from 'src/app/model/ApiQuestion.model';
import { ApiAnswer } from 'src/app/model/ApiAnswer.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class McqService {

  
  constructor(private http: HttpClient) { }
  
    readonly url=Configuration.APIURL +'apiMcq';
    
  GetQuestions(chapterId:number): Observable<ApiQuestion[]>  {  
    let headers = new HttpHeaders();
    headers  = headers.append('responseType', 'json');
    let urlLocal:string;
    urlLocal=this.url + '/getApiQuestions/'+chapterId;
    return this.http.get<ApiQuestion[]>(urlLocal);
  }

  GetAnswers(chapterId:number): Observable<ApiAnswer[]>  {  
    let headers = new HttpHeaders();
    headers  = headers.append('responseType', 'json');
    let urlLocal:string;
    urlLocal=this.url + '/getApiAnswers/'+chapterId;
    return this.http.get<ApiAnswer[]>(urlLocal);
  }
}
