import { Injectable } from '@angular/core';
import {Chapter } from '../../model/chapter.model';
import { Configuration } from 'src/app/configuration/config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChapterService {
  readonly url=Configuration.APIURL +'chapter';

  constructor(private http: HttpClient) { }

  GetChapterList(standardId:string,mediumId:string,subjectId:string) : Observable<Chapter[]> {  
    let headers = new HttpHeaders();
    headers  = headers.append('responseType', 'json');
    let urlLocal:string;
    urlLocal=this.url + '/getchapterlist/'+standardId+'/'+subjectId+'/'+mediumId;
    return this.http.get<Chapter[]>(urlLocal);
  } 
}
