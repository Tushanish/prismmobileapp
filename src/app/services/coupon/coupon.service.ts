import { Injectable } from '@angular/core';
import { Configuration } from 'src/app/configuration/config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Coupon } from 'src/app/model/coupon.model';

@Injectable({
  providedIn: 'root'
})
export class CouponService {
  readonly url=Configuration.APIURL +'coupon';

  constructor(private http: HttpClient) { }

  ValidateCouponCode(couponCode:string) : Observable<Coupon> {  
    let headers = new HttpHeaders();
    headers  = headers.append('responseType', 'json');
    let urlLocal:string;
    urlLocal=this.url + '/validatecoupon/'+couponCode;
    return this.http.get<Coupon>(urlLocal);
  } 
}
