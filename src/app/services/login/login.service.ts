import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';  
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {Login} from '../../model/login.model';
import {Configuration} from '../../configuration/config'

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  readonly url=Configuration.APIURL ;
  token : string;  
  header : any;  

  constructor(private http: HttpClient) {
    const headerSettings: {[name: string]: string | string[]; } = {};  
    this.header = new HttpHeaders(headerSettings);  
  }

  createUser(studentInfo: any,couponId:string) {  
    let student: Login= new Login();
    student.StandardId=Configuration.StandardId.toString();
    student.CouponId=couponId;
    student.DateOfBirth=studentInfo.dateOfBirth.formatted;
    student.MediumId=studentInfo.mediumId.MediumId;
    student.FirstName=studentInfo.firstName;
    student.LastName=studentInfo.lastName;
    student.FatherName=studentInfo.fatherName;
    student.PhoneNumber=studentInfo.phonenumber;
    student.Password=studentInfo.password;
    return this.http.post(this.url+'account/registerstudent', student);  
  }

  ValidateMobileNumber(mobileNumber:string) : Observable<boolean> {  
    let headers = new HttpHeaders();
    headers  = headers.append('responseType', 'json');
    let urlLocal:string;
    urlLocal=this.url + 'account/validateMobileNumber/'+mobileNumber;
    return this.http.get<boolean>(urlLocal);
  } 

  loginUser(phoneNumber:string,password:string,uuid:string) {
    var userData = "username=" + phoneNumber  +"&password=" + password + "&grant_type=password" ;
      var reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded',
      'No-Auth':'True'
      ,'uuid' : uuid 
    });
      return this.http.post(this.url +'account/authenticate',userData,{ headers: reqHeader })
      .pipe(
         map(res => res)
      )
  }

  logOutUser(loginId:string) 
  {
    let urlLocal= this.url + 'account/logoutStudent';
    const url = `${urlLocal}/${loginId}`;
    this.http.get<boolean>(url).subscribe(resp => {
      debugger;
    });
  }
  

  isLoggedIn() {
    if (localStorage.getItem('userinfo')) {
      return true;
    }
    return false;
  }

  getAuthorizationToken() {
    const currentUser = JSON.parse(localStorage.getItem('userinfo'));
    return currentUser.token;
  }

  UpdateAppActiveTime(loginId : number)  
  {
    let headers = new HttpHeaders();
    headers  = headers.append('responseType', 'json');
    let urlLocal:string;
    urlLocal=this.url + 'account/updateAppActiveTime/'+loginId;
    return this.http.post(this.url + 'account/updateAppActiveTime/', loginId);  
  }
}




