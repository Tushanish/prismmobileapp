import { Injectable } from '@angular/core';
import { Configuration } from 'src/app/configuration/config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SubjectSectionContent } from 'src/app/model/subjectsectioncontent.model';

@Injectable({
  providedIn: 'root'
})
export class SubjectcontentService {

  readonly url=Configuration.APIURL +'subjectcontent';
  constructor(private http: HttpClient) { }

  GetSubjectContent(subjectId:string,sectionId:string): Observable<SubjectSectionContent> {  
    let headers = new HttpHeaders();
    headers  = headers.append('responseType', 'json');
    let urlLocal:string;
    urlLocal=this.url + '/getsubjectsectioncontent/'+subjectId+'/'+sectionId;
    return this.http.get<SubjectSectionContent>(urlLocal);
  } 
}
