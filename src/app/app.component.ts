import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoginService } from 'src/app/services/login/login.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit,OnDestroy {
  constructor(private loginService: LoginService) {
    
  }

  ngOnInit() {
    //this.appPauseEvent();
  }

  appPauseEvent()
  {
    document.addEventListener('pause',this.pauseListener , false);
  }


  pauseListener =  () => {
    var currentUserInfo=JSON.parse(localStorage.getItem('userinfo'));
    if(currentUserInfo != null)
    {
        var id = currentUserInfo.LoginId;
        this.updateAppActiveTime(id);
        alert(id);
    }
  } 

   updateAppActiveTime(loginId)
  {
    let response :boolean 
    alert("in api call")
      this.loginService.UpdateAppActiveTime(loginId)
      .subscribe(responseStatus => {
        alert(responseStatus);
      });
      alert("end of api call")
  }


  ngOnDestroy() {
    alert("removing listener")
    document.removeEventListener('pause',this.pauseListener,false);
  }

}
