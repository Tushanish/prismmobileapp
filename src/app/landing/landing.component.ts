import { Component, OnInit } from '@angular/core';
import { Event, Router } from '@angular/router'
import { LoginService } from 'src/app/services/login/login.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { SplashScreen } from '@ionic-native/splash-screen/ngx'
import { File } from '@ionic-native/File/ngx';
import { Platform } from '@ionic/angular';
import { async } from 'rxjs/internal/scheduler/async';

declare let cordova: any;
declare var device : any;

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  public phonenumber: any;
  public password: any;
  private promise: Promise<string>;

  constructor(private route: Router, private formBuilder: FormBuilder,
    private loginService: LoginService,
    private splashScreen: SplashScreen,
    private file: File,
    private platform: Platform) {
      document.addEventListener("deviceready", () => {
        (navigator as NavigatorCordova).splashscreen.show();
      });
  
  }

  async ngOnInit() {
    // await document.addEventListener("deviceready", () => {
    //   (navigator as NavigatorCordova).splashscreen.show();
    // });
    await this.platform.ready().then((x)=> {
      //console.log("calling auto login");
      this.autoLogin();
    });
    // document.addEventListener("deviceready", (x) => {
    //   (navigator as NavigatorCordova).splashscreen.show();
    //   this.platform.ready().then((x) => {
    //     this.autoLogin();
    //   });
    // });
  }
  async autoLogin() {
    await this.readFile('prismCred.txt');
  }

  async readFile(fileName: string) {
    await this.file.checkFile(cordova.file.dataDirectory, fileName).then(
      (file) => {
        this.promise = this.file.readAsText(this.file.dataDirectory, fileName);
         this.promise.then(value => {
          if (value != null || undefined) {
            var values = value.split(" ");
            let phonenumber = values[0];
            let password = values[1];
            this.loginService.loginUser(phonenumber, password,device.uuid)
              .subscribe(
              (res: any) => {
                // console.log("autoLogin Called");
                (navigator as NavigatorCordova).splashscreen.hide();
                var userInfoJson = JSON.stringify(res);
                var userInfo = JSON.parse(userInfoJson);
                if (userInfo.access_token != null) {
                  localStorage.setItem("userinfo", userInfoJson);
                  localStorage.setItem("firstLogin", "true");
                  this.route.navigate(['student/dashboard']);
                }
                else {
                  (navigator as NavigatorCordova).splashscreen.hide();
                  this.route.navigate(['login']);
                }
              },
              err => {
                //console.log("login Service Error");
                //console.log(err.Message);
                (navigator as NavigatorCordova).splashscreen.hide();
                this.route.navigate(['login']);
              });
          }
          else {
            //console.log("login Service Error");
            //console.log(value);
            (navigator as NavigatorCordova).splashscreen.hide();
            this.route.navigate(['']);
          }
        });
      }
    ).catch(
      (err) => {
        //console.log(err.Message);
        //console.log("outer if where check file condition");
        (navigator as NavigatorCordova).splashscreen.hide();
        this.route.navigate(['']);
      }
      );
  }

}
