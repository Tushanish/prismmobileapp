import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  contactNumber : number = 7700008993
  email : string = 'rotaryidealstudy@gmail.com'

  constructor(private routes:Router) { }

  ngOnInit() {
  }

  goToDashboard()
  {
    this.routes.navigate(['/student/dashboard']);
  }

}
