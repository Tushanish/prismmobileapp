import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChaptersectionsComponent } from './chaptersections.component';

describe('ChaptersectionsComponent', () => {
  let component: ChaptersectionsComponent;
  let fixture: ComponentFixture<ChaptersectionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChaptersectionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChaptersectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
