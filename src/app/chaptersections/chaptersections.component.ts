import { Component, OnInit,OnDestroy } from '@angular/core';
import {ChaptersectionService} from '../services/chaptersection/chaptersection.service';
import {Router, ActivatedRoute, NavigationExtras} from '@angular/router';
import { Sections } from '../model/section.model';




@Component({
  selector: 'app-chaptersections',
  templateUrl: './chaptersections.component.html',
  styleUrls: ['./chaptersections.component.css']
})
export class ChaptersectionsComponent implements OnInit {
  private chapterParams: any;
  private fontIcons=['fa-university','fa-bomb','fa-building','fa-child','fa-coffee','fa-cog','fa-cogs'];
  private bgChapterSectionColor=['serviceBox','serviceBox purple',
    'serviceBox blue','serviceBox green']
  bgChapterSectionColorIndex=0;

  chapterSections: Sections[];    
  mediumName:string;
  subjectName:string;
  chapterName:string;
  loading:boolean=true;

  constructor(private routes:Router,
    private routeParam: ActivatedRoute,
  
    private chapterSectionService:ChaptersectionService) { }

  ngOnInit() {
    let standardId:string;
    let mediumId:string;
    let chapterId:string;
    let subjectId:string;


   

    this.chapterParams = this.routeParam.queryParams.subscribe(params => {
      chapterId = params["chapterId"];
      subjectId=params["subjectId"];
    });

    var currentUserInfo=JSON.parse(localStorage.getItem('userinfo'));
    standardId=currentUserInfo.StandardId;
    mediumId=currentUserInfo.MediumId;
    setTimeout( () => {
      this.chapterSectionService.GetChapterSectionList(true,mediumId,true,chapterId,subjectId)
                .subscribe(chapterSections => {
                  this.AssignChapterIdToArray(chapterSections,chapterId);
                  this.AssignChapterSectionBackGroundColor(chapterSections);
                  this.chapterSections = chapterSections;
                  this.subjectName=chapterSections[0].SubjectName;
                  this.chapterName=chapterSections[0].ChapterName;
                  this.mediumName=currentUserInfo.MediumName;
                  this.loading=false;
                });
            }, 2500 );
  }

  AssignChapterIdToArray(tempSection: Sections[],chapterId:string)
  {
    tempSection.map((obj) => {
      obj.ChapterId = chapterId;
      obj.FAIcons = this.getRandomFaIcons();
      return obj;
    })
  }

  getRandomFaIcons(){  
    let fontIcons=['fa fa-university','fa fa-bomb','fa fa-building','fa fa-child','fa fa-coffee','fa fa-cog','fa fa-cogs'];  
    return fontIcons[Math.floor(Math.random() * fontIcons.length)];
  }

  getRandomBgChapterSectionColor(){
    if(this.bgChapterSectionColorIndex==this.bgChapterSectionColor.length) 
    {
      this.bgChapterSectionColorIndex=0;
    }   
    let bgTempColor= this.bgChapterSectionColor[this.bgChapterSectionColorIndex];
    this.bgChapterSectionColorIndex=this.bgChapterSectionColorIndex+1;
    return bgTempColor;
  }

  AssignChapterSectionBackGroundColor(tempSection: Sections[])
  {
    tempSection.map((obj) => {
      obj.ChapterSectionBgColor = this.getRandomBgChapterSectionColor();
      return obj;
    })
  }

  onSectionClick(chapterSection:Sections)
  {
    let navigationExtras: NavigationExtras = {
      queryParams: {
          "chapterId": chapterSection.ChapterId,
          "sectionId": chapterSection.SectionId
      }
    };

    if (chapterSection.SectionId == "10023") {
      this.routes.navigate(['student/mcq'], navigationExtras);
    }
    else
      this.routes.navigate(['student/chaptercontent'], navigationExtras);
  }

 
}


