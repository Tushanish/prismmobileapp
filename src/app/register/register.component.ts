import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder,FormGroup, FormControl, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

// Custom Validation for Password and Confirm Password
import { MustMatch } from '../helpers/must-match.validator';
// import services for database operation 
import { LoginService} from '../services/login/login.service';
import { MediumService } from '../services/medium/medium.service';
// import commom configuration 
import {Configuration} from '../configuration/config'

import { File } from '@ionic-native/File/ngx';
import { Platform } from '@ionic/angular';

import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS } from '../configuration/format-datepicker';
import { CouponService } from '../services/coupon/coupon.service';
import { Coupon } from '../model/coupon.model';
import { IMyDpOptions } from 'mydatepicker';
import { catchError } from 'rxjs/internal/operators/catchError';

declare var jquery:any;
declare var $ :any;
declare let cordova: any;
declare let device : any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
  ]
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  phonenumberpattern = /^(\+\d{1,3}[- ]?)?\d{10}$/;
  coupon:Coupon;
  clicked = false;
  couponCodeText:string;
  couonCodeError:string;
  IsCouponValidated:boolean=false;
  fileName: string = "prismCred.txt";
  private blob: Blob;

  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
  };
  constructor(private routes :Router,
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    public mediumService: MediumService,
    private couponService : CouponService,
    private file: File,
    private platform: Platform) { }
  
  ngOnInit() {
    //this.getDeviceInfo();
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      fatherName: ['', Validators.required],
      mediumId: [null, Validators.required],
      dateOfBirth: [null, Validators.required],
      phonenumber: ['', [Validators.required,Validators.pattern(this.phonenumberpattern)]],
      password: ['', Validators.required],
      confirmpassword: ['', Validators.required]
    }, 
    {
      validator: MustMatch('password', 'confirmpassword') 
    });

    this.mediumService.GetMediumList();
    $("#mediumId").val("0");
    
  }

  // getDeviceInfo()
  // {
  //   alert(device.uuid);
  // }
  setDate(): void {
    let date = new Date();
    this.registerForm.patchValue({dateOfBirth: {
    date: {
        year: date.getFullYear(),
        month: date.getMonth() + 1,
        day: date.getDate()}
    }});
  }

  get f() { return this.registerForm.controls; }

  
  onSubmit() {
    this.submitted = true;
    
    if (this.registerForm.invalid) {
        return;
    }
    else{
      this.clicked = true;
      this.loginService.createUser(this.registerForm.value,this.coupon.CouponId)
            .pipe(first())
            .subscribe(
                data => {
                  try{
                    if(device != undefined)
                    {
                      this.loginService.loginUser(this.registerForm.value.phonenumber, this.registerForm.value.password,device.uuid)
                      .subscribe(
                      (res: any) => {
                        this.OnLogin(res);
                      }
                     );
                    }
                  }
                  catch(err)
                  {
                    this.loginService.loginUser(this.registerForm.value.phonenumber, this.registerForm.value.password,'')
                    .subscribe(
                    (res: any) => {
                      this.OnLogin(res);
                    }
                    );
                  }
                },
                error => {
                 
                });
    }
  }

  
  OnLogin(res : any)
  {
    var userInfoJson = JSON.stringify(res);
    var userCredentials = {
      phonenumber: this.registerForm.value.phonenumber,
      password: this.registerForm.value.password
    }
    this.createFile(this.fileName);

    this.writeFile(this.registerForm.value.phonenumber.toString() + " " + this.registerForm.value.password.toString(), this.fileName);
    var userInfo = JSON.parse(userInfoJson);
    if (userInfo.access_token != null) {
      localStorage.setItem("userinfo", userInfoJson);
      localStorage.setItem("firstLogin", "true");
      this.routes.navigate(['student/dashboard']);
    }
    else {
     // this.displayMessage = "Invalid mobile number and password";
    }
  }

  OnSignIn()
  {
    this.routes.navigate(['login']);
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  } 

  validatePhoneNumber()
  {
    let IsMobileNumberDuplicate:boolean=true;
    let mobileNumber:string=this.registerForm.controls['phonenumber'].value;
    if(mobileNumber.length>0)
    {
      this.loginService.ValidateMobileNumber(mobileNumber)
      .subscribe(couponRecord => {
        IsMobileNumberDuplicate=couponRecord;
      });
    }
    else
    {
      IsMobileNumberDuplicate=false;
    }
   
    return IsMobileNumberDuplicate;
  }

  
  validateCouponCode()
  {
    if(this.couponCodeText==undefined || this.couponCodeText.trim()=='')
    {
      this.IsCouponValidated=false;
      this.couonCodeError="Please enter valid coupon code";
    }
    else
    {
      this.couponService.ValidateCouponCode(this.couponCodeText)
      .subscribe(couponRecord => {
        if(couponRecord!=null)
        {
          this.coupon=couponRecord;
          this.IsCouponValidated=true;
        }
        else
        {
          this.IsCouponValidated=false;
          this.couonCodeError="Please enter valid coupon code";
        }
      });
    }
  }

  createFile(fileName: string) {
    this.file.createFile(this.file.dataDirectory, fileName, true);
  }

  async writeFile(stringToRight: string, fileName: string) {
    await this.platform.ready();
    this.blob = new Blob([stringToRight], { type: 'text/plain' });
    this.file.writeFile(this.file.dataDirectory, fileName, this.blob, { replace: true, append: false });
  }
}
