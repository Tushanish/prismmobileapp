import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectcontentComponent } from './subjectcontent.component';

describe('SubjectcontentComponent', () => {
  let component: SubjectcontentComponent;
  let fixture: ComponentFixture<SubjectcontentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectcontentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectcontentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
