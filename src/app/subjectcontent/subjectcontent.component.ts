import { Component, OnInit, PipeTransform, Pipe } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SubjectSectionContent } from '../model/subjectsectioncontent.model';
import {SubjectcontentService} from '../services/subjectcontent/subjectcontent.service'
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-subjectcontent',
  templateUrl: './subjectcontent.component.html',
  styleUrls: ['./subjectcontent.component.css']
})

@Pipe({ name: 'safeHtml'})
// export class SafeHtmlPipe implements PipeTransform  {
//   constructor(private sanitized: DomSanitizer) {}
//   transform(value) {
//     return this.sanitized.bypassSecurityTrustHtml(value);
//   }
// }

export class SubjectcontentComponent implements OnInit {
  private subjectSectionParams: any;
  subjectSectionContent: SubjectSectionContent;
  loading:boolean=true;

  constructor(private routes:Router,
    private routeParam: ActivatedRoute,
    private subjectContentService:SubjectcontentService) { }

  ngOnInit() {

    let subjectId:string;
    let sectionId:string;

    this.subjectSectionParams = this.routeParam.queryParams.subscribe(params => {
      subjectId = params["subjectId"];
      sectionId = params["sectionId"];
    });

    this.subjectContentService.GetSubjectContent(subjectId,sectionId)
              .subscribe(_subjectSectionContent => {
                this.subjectSectionContent=_subjectSectionContent;
                this.loading=false;
              });
  }

}
