import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LandingComponent } from './landing/landing.component'
import { StudentComponent } from './layouts/student/student.component';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserprofileComponent } from './userprofile/userprofile.component';
import { ChapterComponent } from './chapter/chapter.component';
import { ChaptersectionsComponent } from './chaptersections/chaptersections.component';
import { ChaptercontentComponent } from './chaptercontent/chaptercontent.component';
import { SubjectSectionContent } from './model/subjectsectioncontent.model';
import { SubjectcontentComponent } from './subjectcontent/subjectcontent.component';
import { McqComponent } from 'src/app/mcq/mcq.component';
import { ContactUsComponent } from 'src/app/contact-us/contact-us.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';

const routes: Routes = [
  { path:'', component: LandingComponent },
  { path:'privacypolicy', component:PrivacyPolicyComponent},
  { path:'login', component: LoginComponent },
  { path:'register', component: RegisterComponent },
  { 
    path:'student', 
    component: StudentComponent,data: {
      breadcrumb: 'Dashboard',
    },
    children: [
      { path: '', component: DashboardComponent},
      
      { path: 'dashboard', component: DashboardComponent},
      { path: 'userprofile', component: UserprofileComponent},
      { path: 'chapter', component: ChapterComponent},
      { path: 'chaptersections', component: ChaptersectionsComponent},
      { path: 'chaptercontent', component: ChaptercontentComponent },
      { path: 'subjectcontent', component: SubjectcontentComponent },
      { path: 'mcq', component: McqComponent },
      { path: 'contact-us', component: ContactUsComponent },
    ]
  },
 ];

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
