import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { RouterModule,Routes } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { NumericonlyDirective } from './directives/numericonly.directive';
import { MatSelectionListMultipleDirective } from './directives/listmultiple.directive'
import { HttpClientModule } from '@angular/common/http';


import { SplashScreen } from '@ionic-native/splash-screen/ngx'
import { File } from '@ionic-native/File/ngx';

import {LoginService} from './services/login/login.service';
import {BrowserAnimationsModule } from '@angular/platform-browser/animations';
// Angular material
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule, MatNativeDateModule,MatListModule,MatCardModule, MatLineModule, MatIconModule, MatButtonModule } from '@angular/material';
import {MatTooltipModule} from '@angular/material/tooltip';
import { MatVideoModule } from 'mat-video';

import { CountdownModule } from 'ngx-countdown'

import { StudentComponent } from './layouts/student/student.component';

import { NavbarModule } from './shared/navbar/navbar.module';
import { FooterModule } from './shared/footer/footer.module';
import { SidebarModule } from './sidebar/sidebar.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserprofileComponent } from './userprofile/userprofile.component';
import { ChapterComponent } from './chapter/chapter.component';
import { ChaptersectionsComponent } from './chaptersections/chaptersections.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { ChaptercontentComponent, SafeHtmlPipe } from './chaptercontent/chaptercontent.component';

import { Angular2UsefulSwiperModule } from 'angular2-useful-swiper';
import { SubjectcontentComponent } from './subjectcontent/subjectcontent.component';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { McqComponent } from './mcq/mcq.component';
import { MyDatePickerModule } from 'mydatepicker';

import { cordova, cordovaFunctionOverride } from '@ionic-native/core';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    LandingComponent,
    NumericonlyDirective,
    MatSelectionListMultipleDirective,
    StudentComponent,
    DashboardComponent,
    UserprofileComponent,
    ChapterComponent,
    ChaptersectionsComponent,
    BreadcrumbComponent,
    ChaptercontentComponent,
    SubjectcontentComponent,
    SafeHtmlPipe,
    McqComponent,
    ContactUsComponent,
    PrivacyPolicyComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,       
    MatNativeDateModule,        
    MatTooltipModule,
    MatListModule,
    MatCardModule,
    MatVideoModule,
    MatLineModule,
    MatIconModule,
    CountdownModule,
    AppRoutingModule,
    NavbarModule ,
    FooterModule ,
    SidebarModule,
    Angular2UsefulSwiperModule,
    LazyLoadImageModule,
    MyDatePickerModule
  ],
  providers: [LoginService,SplashScreen,File],
  bootstrap: [AppComponent]
})
export class AppModule {
}
