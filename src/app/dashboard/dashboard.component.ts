import { Component, OnInit, OnDestroy  } from '@angular/core';
import { Router, NavigationExtras, NavigationEnd, NavigationStart } from '@angular/router';
import { SubjectService } from '../services/subject/subject.service';
import { Subject } from '../model/subject.model';
import { Configuration } from '../configuration/config';
import { LoginService } from 'src/app/services/login/login.service';

  
declare var jquery:any;
declare var $ :any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
  // BreadCrumb Variables 
  mediumName:string;

  // Subjest List Data
  subjectList:Subject[]; 

  // Display Loading  .substr(2)
  loading:boolean=true;

  // Display Message if any
  message:string;

  SPThumbnailImage: string;

  public baseUrl=Configuration.APIURL;


  SPLandingImage:string;
  SponsorerName:string;
  previousUrl: string;
  IsPreviousUrlLogin:boolean=false;
  constructor(private routes :Router,
    private subjectService: SubjectService,
  private loginService:LoginService) { 
      
    }

  ngOnInit() {
   

    this.goBackAlert();

    let loginId:string= this.getUserInfoDetails().LoginId;
    let mediumId:string= this.getUserInfoDetails().mediumId;
    this.mediumName=this.getUserInfoDetails().MediumName;
    if(this.getUserInfoDetails().SponsorerName!=null)
    {
      this.SPLandingImage = this.baseUrl +''+ this.getUserInfoDetails().SPLandingImage.substr(2);
      this.SPThumbnailImage = this.baseUrl + '' + this.getUserInfoDetails().SPThumbnailImage.substr(2);
      this.SponsorerName = this.getUserInfoDetails().SponsorerName;
      
    }
    else
    {
      this.SPLandingImage ='assets/Thumbnail/eballogo.gif';
      this.SponsorerName = 'Prosim Solution PVT. LTD.';
    }
    $(".toggled").click();
    this.getSubjectList(loginId);
    var firstTimeLogin=localStorage.getItem('firstLogin');
    if(firstTimeLogin=="true")
    {
      localStorage.setItem("firstLogin","false");
      $("#myModal").modal('show');
    }
    
  }


  //#region get subject list from service
  getSubjectList(loginId:string)
  {
    this.subjectService.GetSubjectList(loginId)
    .subscribe(
      response => {
        this.onProcessSubjectList(response)
      }, 
      error => {
        this.message="Some internal server occurs";
      }
    );
    this.loading=false;
  }
  //#endregion get subject list from service

  //#region process subject list 
  onProcessSubjectList(subjects:Subject[])
  {
    // Assign background color 
    this.assignBackGround(subjects);
    this.subjectList = subjects;
  }
  //#endregion process subject list
  

  //#region Assign background color to subject list
  private bgColor=['serviceBox color3','serviceBox green'
  ,'serviceBox blue','serviceBox color1','serviceBox color2'
  ,'serviceBox','serviceBox purple'];
  bgColorIndex=0;

  assignBackGround(tempSubject: Subject[])
  {
    tempSubject.map((obj) => {

      if(this.bgColorIndex==this.bgColor.length) 
      {
        this.bgColorIndex=0;
      }   
      let bgTempColor= this.bgColor[this.bgColorIndex];
      this.bgColorIndex=this.bgColorIndex+1;

      obj.SubjectBgColor = bgTempColor;
      
      if(obj.SubjectThumbnailPath!=null)
      {
        obj.SubjectThumbnailPath = this.baseUrl +''+ obj.SubjectThumbnailPath.substr(2);
      }
      else
      {
        obj.SubjectThumbnailPath ='assets/Thumbnail/eballogo.gif';
      }

      return obj;
    })
  }
  //#endregion Assign background color to subject list

  //#region On click navigate to chapter list
  onSubjectClick(subject:Subject)
  {
    let navigationExtras: NavigationExtras = {
      queryParams: {
          "subjectId": subject.SubjectId
      }
    };
    this.routes.navigate(['student/chapter'], navigationExtras);
  }
  //#endregion On click navigate to chapter list

  //#region Get logged user details 
  getUserInfoDetails()
  {
    var currentUserInfo=JSON.parse(localStorage.getItem('userinfo'));
    if(currentUserInfo==null)
    {
      this.routes.navigate(['']);
    }
    return currentUserInfo;
  }

  yesClick() {
    (navigator as NavigatorCordova).app.exitApp();
  }

  noClick() {
    $("#exitScreenModal").modal('hide');
  } 

  goBackAlert()
  {
    var currentUrl = this.routes.url;
    if (currentUrl === '/student/dashboard') {
      // console.log(currentUrl)
      
      document.addEventListener('backbutton',this.listener , false);
    }
  }

   listener =  () => {
    if ($($('.navbar-toggler')[0]).hasClass('toggled')) {
      $($('.navbar-toggler')[0]).click()
     }
     else 
     {
      $("#exitScreenModal").modal('show');
     }
  } 

  ngOnDestroy() {
    document.removeEventListener('backbutton',this.listener,false);
  }
  //#endregion Get logged user details 
}
