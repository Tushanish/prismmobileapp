import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';

import { LoginService } from '../services/login/login.service';
import { File } from '@ionic-native/File/ngx';
import { Platform } from '@ionic/angular';
import { PreviousRouteService } from 'src/app/services/previousroute/previousroute.service';
import { window } from 'rxjs/internal/operators/window';

declare var jquery: any;
declare var $: any;
declare let cordova: any;
declare var device : any;

const CRED_NO = 'credentials_phoneNumber';
const CRED_PASS = 'credentials_password';

@Component({
  selector: 'loginComponent',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [PreviousRouteService]
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  submitted = false;
  phonenumberpattern = /^(\+\d{1,3}[- ]?)?\d{10}$/;
  displayMessage = "";
  fileName: string = "prismCred.txt";

  public phonenumber: any;
  public password: any;
  private blob: Blob;

  public filePath: string;
  constructor(private route: Router, private formBuilder: FormBuilder,
    private loginService: LoginService,
    private file: File,
    private platform: Platform,
    private previousUrl: PreviousRouteService
  ) {

  }

  ngOnInit() {
    //this.getDeviceInfo();
    this.checkExitApp();
    this.loginForm = this.formBuilder.group({
      phonenumber: ['', [Validators.required, Validators.pattern(this.phonenumberpattern)]],
      password: ['', Validators.required]
    });
    this.filePath = this.file.dataDirectory;
    
  }

  // getDeviceInfo()
  // {
  //   alert(device.uuid);
  // }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    else {
      try {
        if(device !=undefined)
        {
          this.onLogin(device.uuid)
        }
      }
      catch(err)
      {
        this.onLogin('');
      }
    }

  }

  
  onLogin(deviceId:string)
  {
    this.loginService.loginUser(this.loginForm.value.phonenumber, this.loginForm.value.password,deviceId)
        .subscribe(
        (res: any) => {

          var userInfoJson = JSON.stringify(res);
          var userCredentials = {
            phonenumber: this.loginForm.value.phonenumber,
            password: this.loginForm.value.password
          }
          this.createFile(this.fileName);
          this.writeFile(this.loginForm.value.phonenumber.toString() + " " + this.loginForm.value.password.toString(), this.fileName);
          var userInfo = JSON.parse(userInfoJson);
          if (userInfo.access_token != null) {
            localStorage.setItem("logOutClicked","false");
            localStorage.setItem("userinfo", userInfoJson);
            localStorage.setItem("firstLogin", "true");
            this.route.navigate(['student/dashboard']);
          }
          else {
            this.displayMessage = "Invalid mobile number and password";
          }
        },
        err => {
          if(err.error.error == "duplicate_login")
          {
            this.displayMessage = "User already logged in on another device. Please logout from other device";
          }
          else 
          {
            this.displayMessage = "Invalid mobile number and password";
          }
        });
  }
  OnSignUp() {
    this.route.navigate(['register'])
  }

  createFile(fileName: string) {
    this.file.createFile(this.file.dataDirectory, fileName, true);
  }

  checkExitApp() {
    var logOutValue = JSON.parse(localStorage.getItem('logOutClicked'));
    if (logOutValue != undefined && logOutValue != 'false') {
      document.addEventListener('backbutton', this.listener, false);
    }
  }

  async writeFile(stringToRight: string, fileName: string) {
    await this.platform.ready();
    this.blob = new Blob([stringToRight], { type: 'text/plain' });
    this.file.writeFile(this.file.dataDirectory, fileName, this.blob, { replace: true, append: false });
  }

  listener = () => {
    var logOutValue = JSON.parse(localStorage.getItem('logOutClicked'));
    if (logOutValue != undefined && logOutValue != 'false') {
      (navigator as NavigatorCordova).app.exitApp();
    }
  }

  ngOnDestroy() {
    document.removeEventListener('backbutton', this.listener, false);
  }

}
