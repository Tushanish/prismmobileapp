import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { ChaptercontentService } from 'src/app/services/chaptercontent/chaptercontent.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSelectionList, MatListOption, MatSelectionListChange } from '@angular/material';
import {
  CountdownGlobalConfig, CountdownComponent, CountdownConfig, CountdownEvent,
  CountdownEventAction, CountdownItem
}
  from 'ngx-countdown'
import { McqService } from 'src/app/services/mcq/mcq.service';
import { ApiQuestion } from 'src/app/model/ApiQuestion.model';
import { ApiAnswer } from 'src/app/model/ApiAnswer.model';
import { Location } from '@angular/common';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-mcq',
  templateUrl: './mcq.component.html',
  styleUrls: ['./mcq.component.css'],
  providers: [CountdownGlobalConfig]
})
export class McqComponent implements OnInit,OnDestroy {

  private chapterSectionParams: any;
  allAnswers: ApiAnswer[];
  loading: boolean = true;
  time: string;
  questions: ApiQuestion[];
  firstQuestion: boolean;
  disabled: boolean = false;
  hideSave: boolean;
  selectedOptions: any;
  currentQuestionId: number = 0;
  filteredQuestion: ApiQuestion;
  filteredAnswer: Array<ApiAnswer>[];
  currentQuestion: string;
  totalQuestions: number;
  finalAnswers: any[] = [];
  showScore : boolean = false;
  finalScore : string;
  emojiPath : string;
  mcqNotConfigured :boolean;
  showAnswers : boolean;
  lastQuestion : boolean;

  constructor(
    private routes: Router,
    private routeParam: ActivatedRoute,
    private location:Location,
    private chapterContentService: ChaptercontentService,
    private mcqService: McqService,
  ) { }

  ngOnInit() {
    let chapterId: string;
    let sectionId: string;
    this.loading = false;
    this.mcqNotConfigured = false;
    this.hideSave = false;
    this.firstQuestion = true;
    this.showAnswers = false;
    this.lastQuestion = false;
    this.goEventHandler();

    this.chapterSectionParams = this.routeParam.queryParams.subscribe(params => {
      chapterId = params["chapterId"];
      sectionId = params["sectionId"];
    });
    this.mcqService.GetQuestions(Number(chapterId)).subscribe(res => {
      this.questions = res;
      if(this.questions == undefined || this.questions.length == 0)
      {
          this.mcqNotConfigured = true;
          return;
      }
      this.currentQuestionId = this.questions[0].QuestionId;
      this.isFirstQuestion();
      this.getTotalQuestions();
      this.mcqService.GetAnswers(Number(chapterId)).subscribe(answers => {
        this.allAnswers = answers
        this.deselectExceptSelected(0,0,true);
        this.getQuestion(false,false);
      });
    });
    $("#myModal").modal('show');
    
  }

 
  isFirstQuestion() {
    var firstQuestionId = this.questions[0];
    // var firstQuestionId = this.questions.reduce((a, b) => a.QuestionId < b.QuestionId ? a : b)
    if (this.currentQuestionId === firstQuestionId.QuestionId) {
      this.firstQuestion = true;
    }
    else {
      this.firstQuestion = false;
    }
  }

  isLastQuestion()
  {
    var lastQuestionId = this.questions[this.questions.length - 1];
    // var lastQuestionId = this.questions.reduce((a, b) => a.QuestionId > b.QuestionId ? a : b)
    if(this.currentQuestionId === lastQuestionId.QuestionId)
    {
      this.lastQuestion = true;
    }
    else 
    {
      this.lastQuestion = false;
    }

  }

  getNextQuestion() {
    if(this.firstQuestion) this.firstQuestion = false;
    this.getQuestion(false,false);
  }

  getPreviousQuestion() {
    this.getQuestion(true,false);
  }

  getQuestion(prev: boolean, current :boolean ) {
    this.clearAnswerArray();
    let question: ApiQuestion;
    let answer : ApiAnswer[];
    let index : number ;
    if(current || this.firstQuestion)
    {
      question = this.questions.find(x => x.QuestionId === this.currentQuestionId);
      answer = this.allAnswers.filter(x => x.QuestionId === this.currentQuestionId);
      index = this.questions.findIndex(x => x.QuestionId === this.currentQuestionId)  ;
    }
    else 
    {
      // question = this.questions.filter(x => x.QuestionId === (prev ? this.currentQuestionId - 1 : this.currentQuestionId + 1));
      
      
      if(prev)
      {
        index = this.questions.findIndex(x => x.QuestionId === this.currentQuestionId);
         question = (this.questions[index - 1] !== undefined && this.questions[index - 1] != null) ? 
         this.questions[index - 1] : this.questions[index] ;
      }
      else 
      {
        index = this.questions.findIndex(x => x.QuestionId === this.currentQuestionId);
          question = (this.questions[index + 1] !== undefined && this.questions[index + 1] != null) ? 
          this.questions[index + 1] : this.questions[index] ;

      }
      this.currentQuestionId = question.QuestionId;
      answer = this.allAnswers.filter(x => x.QuestionId === this.currentQuestionId);
    }
   
    
    this.filteredQuestion = new ApiQuestion(question.QuestionId, question.QuestionValue);
    this.filteredAnswer.push(answer);

    this.currentQuestionId = question.QuestionId;

  
    index = this.questions.findIndex(x => x.QuestionId === this.currentQuestionId);
    index += 1;
    this.currentQuestion =    index.toString() + " Of " + this.totalQuestions.toString();
    
    this.isFirstQuestion();
    this.isLastQuestion();
  }
  enableAllButtons() {
    this.disabled = false;
    this.hideSave = true;
  }

  onNgModelChange($event) {
    this.selectedOptions = $event;
  }

  onSelection(e, v){
    this.selectedOptions = v.selected.map(item => item.value);
    let answer = this.allAnswers.filter(x => x.QuestionId == this.currentQuestionId 
      && x.AnswerValue == this.selectedOptions);
    this.setSelectedAnswer(answer[0]);
  }

  getTotalQuestions() {
    this.totalQuestions = this.questions.length;
  }

  saveMcq() {
    let answer = this.allAnswers.filter(x => x.QuestionId == this.currentQuestionId && x.AnswerValue == this.selectedOptions);
    this.setSelectedAnswer(answer[0]);
  }


  clearAnswerArray() {
    this.filteredAnswer = [];
  }

  setSelectedAnswer(answer: ApiAnswer) {
    if (this.allAnswers.length > 0 && this.allAnswers != undefined) {
      let answerFinal: ApiAnswer[] = this.allAnswers.filter(x => x.QuestionId == this.currentQuestionId && x.AnswerValue == answer.AnswerValue)
      if ((answerFinal != null || undefined) && answerFinal.length > 0) {
        answerFinal[0].SelectedValue = true;
        this.deselectExceptSelected(this.currentQuestionId, answerFinal[0].AnswerId, false);
      }
    }

  }

  deselectExceptSelected(questionId: number, answerId: number, deselectAll: boolean) {
    this.allAnswers.forEach(element => {
      if (element.QuestionId == questionId && element.AnswerId != answerId && deselectAll == false)
        element.SelectedValue = false;

      if (deselectAll)
        element.SelectedValue = false;
    });
  }

  submitMcq() {
    this.disabled = true;
    this.showScore = true;
    this.calculateScore();
  }

  calculateScore()
  {
    this.finalScore = this.allAnswers.
    filter(x => x.SelectedValue && x.IsCorrectAnswer).length.toString();
    let score = Number(this.finalScore);
    if(score >= 8)
    {
      this.emojiPath = "assets/images/thumbs_up.jpg";
    }

    if(score >= 5 && score < 8)
    {
      this.emojiPath = "assets/images/happy_emoji.png";
    }

    if(score < 5)
    {
      this.emojiPath = "assets/images/sad_face.png";
    }

  }

  viewAnswers()
  {
    this.showAnswers = true;
    this.showScore = false;
    this.currentQuestionId = this.questions[0].QuestionId;
    this.getQuestion(false,true);
  }

  
  goEventHandler()
  {
      document.addEventListener('backbutton',this.listener , false);
  }

  listener = () => {
     if($("#myModal").hasClass('show'))
      {
        $("#myModal").modal('hide')
        
      }
      else 
       this.location.back();
  } 

  ngOnDestroy()
  {
    document.removeEventListener('backbutton',this.listener,false);
  }

}


