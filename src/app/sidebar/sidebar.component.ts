import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { async } from 'rxjs/internal/scheduler/async';
import { File } from '@ionic-native/File/ngx';
import { LoginService } from 'src/app/services/login/login.service';

declare var jquery: any;
declare var $: any;
declare let cordova: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit,OnDestroy {
  
  step: string;
  private blob: Blob;
  public filePath: string;

  constructor(private route: Router,
    private platform: Platform,
    private loginService:LoginService,
    private file: File
  ) { }

  ngOnInit() {
    this.step = "dashboard";
    //this.goEventHandler();
  }

  onSignOut() {
    $(".toggled").click();
    var currentUserInfo=JSON.parse(localStorage.getItem('userinfo'));
    this.loginService.logOutUser(currentUserInfo.LoginId);
    localStorage.removeItem("userinfo")
    this.writeFile("",'prismCred.txt')
    this.route.navigate(['login'])
  }

  async writeFile(stringToRight: string, fileName: string) {
    await this.file.checkFile(cordova.file.dataDirectory, fileName).then(
      (file) => {
        this.file.removeFile(cordova.file.dataDirectory, fileName)
        localStorage.setItem("logOutClicked", "true");
      });
  }

  listener = () => {
    if ($( '.toggled' ).is( ":visible" )) {
      $(".toggled").removeClass('active')
      $(".toggled").removeClass('toggled')
     }
    }

  goEventHandler() {
    //document.addEventListener('backbutton',this.listener , false);
  }

  ngOnDestroy()
  {
    //document.removeEventListener('backbutton',this.listener,false);
  }

}
