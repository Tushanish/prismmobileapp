export class Login
{
    LoginId: number;
    FirstName :string;
    FatherName :string;
    LastName: string ;
    DateOfBirth:string;
    PhoneNumber: string;
    Password: string;
    MediumId:string;
    StandardId:string;
    CouponId:string;
    Token: string;
}