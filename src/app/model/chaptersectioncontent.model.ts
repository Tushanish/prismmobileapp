export class ChapterSectionContent{
    SectionId :string;
    SectionName :string;
    ContentType:string;
    ChapterId:string;
    ChapterName :string;
    SubjectId :string;
    SubjectName :string;
    ChapterImageContents :string;
    ChapterVideoContents:string;
    ChapterTextContents :string;
    StandardId :string;
    StandardName :string;
    MediumId :string;
    MediumName :string;
}
 