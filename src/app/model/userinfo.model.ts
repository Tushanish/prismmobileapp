export class UserInfo
{
    constructor(access_token: string, 
        token_type: string,
        name: string, 
        phoneNumber: string,
        loginId:string
        ) {
            this.AccessToken=   access_token;
            this.TokenType=   token_type;
            this.FullName=   name;
            this.PhoneNumber=   phoneNumber;
            this.LoginId=   loginId;

    }
    AccessToken: string;
    TokenType: string;
    FullName:string;
    PhoneNumber: string;
    LoginId:string;
}