export class ApiQuestion
{
    constructor(questionId : number, questionValue : string) {
        this.QuestionId = questionId,
        this.QuestionValue = questionValue
    }
    QuestionId : number;
    QuestionValue : string;
}