export class ChapterSection{
    SectionId:string;
    SectionName:string;
    MediumId:string;
    ChapterId:string;
    StandardId:string;
    SubjectId:string;
}