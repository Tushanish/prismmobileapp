export class ApiAnswer
{
  
    constructor(answerId : number, questionId : number, answerValue : string,selecteValue : boolean, correctAnswer : boolean ) {
        this.AnswerId = answerId;
        this.QuestionId = questionId ;
        this.AnswerValue = answerValue;
        this.SelectedValue = selecteValue;
        this.IsCorrectAnswer = correctAnswer;

    }
    AnswerId : number;
    QuestionId : number;
    AnswerValue : string;
    SelectedValue : boolean;
    IsCorrectAnswer : boolean;
}