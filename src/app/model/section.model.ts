export class Sections
{
    ChapterId:string;
    ChapterName:string;
    StandardId:string;
    SectionId:string;
    SectionName: string;
    SubjectId:string;
    SubjectName:string;
    FAIcons:string;
    StandardSectionBgColor:string;
    ChapterSectionBgColor:string;
}