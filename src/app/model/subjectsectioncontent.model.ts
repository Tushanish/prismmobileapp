export class SubjectSectionContent{
    SubjectContentId :string
    SectionId :string
    SectionName :string

    StandardId :string
    StandardName :string

    SubjectId :string
    SubjectName :string

    MediumId :string
    MediumName :string

    SubjectTextContents :string
}