export class Chapter
{
    ChapterId:string;
    ChapterName:string;
    ChapterThumbnailPath:string;
    SubjectId:string;
    SubjectName:string;
    ChapterBgColor:string;
}