interface NavigatorCordova extends Navigator {
    app: {
      exitApp: () => any;
      backHistory: () => any;
      
    }
    splashscreen:
    {
      hide : () => any;
      show : () => any;
    }
  }