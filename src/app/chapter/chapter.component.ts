import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, NavigationExtras} from '@angular/router';
import {ChapterService} from '../services/chapter/chapter.service';
import { Chapter } from '../model/chapter.model';
import { Sections } from '../model/section.model';
import { StandardsectionService } from '../services/standardsection/standardsection.service';
import { SwiperOptions } from 'swiper';
import { Configuration } from '../configuration/config';

@Component({
  selector: 'app-chapter',
  templateUrl: './chapter.component.html',
  styleUrls: ['./chapter.component.css']
})
export class ChapterComponent implements OnInit {
  private subjectParams: any;
  fontAwsomeIcons:String;
  mediumName:string;
  subjectName:string;
  chapterList:Chapter[]; 
  sectionList:Sections[];

  standardId:string;
  mediumId:string;
  subjectId:string;

  loading:boolean=true;

  private bgChapterColor=['serviceBoxChapter darkblue'
  ,'serviceBoxChapter yellow','serviceBoxChapter red'];
  bgChapterColorIndex=0;

  private bgStandardSectionColor=['serviceBoxStandardSection','serviceBoxStandardSection purple',
    'serviceBoxStandardSection blue','serviceBoxStandardSection green']
  bgStandardSectionColorIndex=0;

  config: SwiperOptions = {
    pagination: { el: '.swiper-pagination', clickable: true },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    initialSlide:0,
    spaceBetween: 10,
    slidesPerView: 1
  };

  public baseUrl=Configuration.APIURL;
  
  constructor(private routes:Router,
    private routeParam: ActivatedRoute,
    private chapterService:ChapterService,
    private standardSectionService: StandardsectionService) { 
  }

  ngOnInit() {
   

    this.subjectParams = this.routeParam.queryParams.subscribe(params => {
      this.subjectId = params["subjectId"];
    });

    var currentUserInfo=JSON.parse(localStorage.getItem('userinfo'));
    this.standardId=currentUserInfo.StandardId;
    this.mediumId=currentUserInfo.MediumId;
    setTimeout( () => {
    this.chapterService.GetChapterList(this.standardId,this.mediumId,this.subjectId)
              .subscribe(chapterList => {
                this.AssignChapterBackGroundColor(chapterList);
                this.chapterList = chapterList;
                this.mediumName=currentUserInfo.MediumName;
                if(chapterList.length>0)
                {
                  this.subjectName=chapterList[0].SubjectName
                }
                else{
                  this.subjectName='';
                }

                
              });
          }, 2500 );

    setTimeout( () => {
    this.standardSectionService.GetStandardSectionList(true,this.subjectId,this.mediumId,true)
              .subscribe(sectionList => {
                this.AssignFAIConsArray(sectionList);
                this.AssignStandardSectionBackGroundColor(sectionList)
                this.sectionList = sectionList;
                this.mediumName=currentUserInfo.MediumName;
                if(sectionList.length>0)
                {
                  this.subjectName=sectionList[0].SubjectName
                }
                else{
                  this.subjectName='';
                }
                this.loading=false;
              });
            }, 2500 );
    
  }

  onChapterClick(chapter:Chapter)
  {
    let navigationExtras: NavigationExtras = {
      queryParams: {
          "chapterId": chapter.ChapterId,
          "subjectId": chapter.SubjectId
      }
    };
    this.routes.navigate(['student/chaptersections'], navigationExtras);
  }

  AssignFAIConsArray(tempSection: Sections[])
  {
    tempSection.map((obj) => {
      obj.FAIcons = this.getRandomFaIcons();
      return obj;
    })
  }

  getRandomFaIcons(){  
    let fontIcons=['fa fa-university','fa fa-bomb','fa fa-building','fa fa-child','fa fa-coffee','fa fa-cog','fa fa-cogs'];  
    return fontIcons[Math.floor(Math.random() * fontIcons.length)];
  }

  getRandomBgChapterColor(){
    if(this.bgChapterColorIndex==this.bgChapterColor.length) 
    {
      this.bgChapterColorIndex=0;
    }   
    let bgTempColor= this.bgChapterColor[this.bgChapterColorIndex];
    this.bgChapterColorIndex=this.bgChapterColorIndex+1;
    return bgTempColor;
  }

  AssignChapterBackGroundColor(tempChapter: Chapter[])
  {
    tempChapter.map((obj) => {
      obj.ChapterBgColor = this.getRandomBgChapterColor();
      if(obj.ChapterThumbnailPath!=null)
      {
        obj.ChapterThumbnailPath = this.baseUrl +''+ obj.ChapterThumbnailPath.substr(2);
      }
      else
      {
        obj.ChapterThumbnailPath ='assets/Thumbnail/eballogo.gif';
      }
      return obj;
    })
  }

  getRandomBgStandardSectionColor(){
    if(this.bgStandardSectionColorIndex==this.bgStandardSectionColor.length) 
    {
      this.bgStandardSectionColorIndex=0;
    }   
    let bgTempColor= this.bgStandardSectionColor[this.bgStandardSectionColorIndex];
    this.bgStandardSectionColorIndex=this.bgStandardSectionColorIndex+1;
    return bgTempColor;
  }

  AssignStandardSectionBackGroundColor(tempChapter: Sections[])
  {
    tempChapter.map((obj) => {
      obj.StandardSectionBgColor = this.getRandomBgStandardSectionColor();
      return obj;
    })
  }

  onSubjectSectionClick(standardSection:Sections){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          "subjectId": this.subjectId,
          "sectionId": standardSection.SectionId
      }
    };
    this.routes.navigate(['student/subjectcontent'], navigationExtras);
  }
  
  
}
