import { Component } from '@angular/core';
import { Router } from '@angular/router';

declare var $:any;

@Component({
    selector: 'footer-cmp',
    templateUrl: 'footer.component.html'
})

export class FooterComponent{
    sponsorerName : string = "Ideal Study";
    constructor(private routes :Router) {
    }
    ngOnInit() {
    if (this.getUserInfoDetails().SponsorerName!=null)
    {
      this.sponsorerName = this.getUserInfoDetails().SponsorerName;
    }
}

    getUserInfoDetails()
    {
      var currentUserInfo=JSON.parse(localStorage.getItem('userinfo'));
      if(currentUserInfo==null)
      {
        this.routes.navigate(['']);
      }
      return currentUserInfo;
    }

}
