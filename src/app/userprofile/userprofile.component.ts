import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent implements OnInit {

  constructor(private routes:Router,
    private routeParam: ActivatedRoute) { }

    loginId:String;
    mediumName:String;
    fullName:String;
    phoneNumber:string
    standardName:string

  ngOnInit() {
    $(".navbar-toggler").click();
    //$(".toggled").click();
    this.loginId = this.getUserInfoDetails().LoginId;
    this.mediumName = this.getUserInfoDetails().MediumName;
    this.fullName = this.getUserInfoDetails().Name;
    this.phoneNumber = this.getUserInfoDetails().PhoneNumber;
    this.standardName = this.getUserInfoDetails().StandardId === "1" ? "10th Standard" : "";
  }


  getUserInfoDetails() {
    var currentUserInfo = JSON.parse(localStorage.getItem('userinfo'));
    if (currentUserInfo == null) {
      this.routes.navigate(['']);
    }
    return currentUserInfo;
  }

  goToDashboard()
  {
    this.routes.navigate(['/student/dashboard']);
  }

}
